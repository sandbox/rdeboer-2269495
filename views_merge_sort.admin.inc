<?php

/**
 * @file
 * views_merge_sort.admin.inc
 */

/**
 * Module configuration settings form.
 *
 * @param array $form
 *   Form stub to be fleshed out by this function.
 * @param array $form_state
 *   The form state.
 *
 * @return array
 *   The updated form.
 */
function views_merge_sort_settings_form($form, &$form_state) {
  $form = array();

  $form['views'] = array(
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#title' => t('Apply merge-sort'),
  );

  $options = array('' => t('- None -'));
  foreach (views_get_all_views() as $view_id => $view) {
    foreach ($view->display as $display_id => $display) {
      if (isset($display->display_options['sorts'])) {
        if (count($display->display_options['sorts']) >= 2) {
          $options["$view_id:$display_id"] = _views_merge_sort_get_display_name($view, $display);
        }
      }
    }
  }
  $default = variable_get('views_merge_sort_views');
  $form['views']['views_merge_sort_views'] = array(
    '#type' => 'select',
    '#multiple' => TRUE,
    '#size' => min(count($options), 10),
    '#title' => t('Views to apply merge-sort to'),
    '#options' => $options,
    '#default_value' => empty($default) ? array('') : $default,
    '#description' => t('View displays with two or more sort criteria are eligible and shown above.') . '<br/>' .
      t('It is up to you to decide if these views are suitable for merge-sort in your application.'),
  );
  return system_settings_form($form);
}

/**
 * Returns an informative display string for the sort(s) of a view display.
 *
 * @param object $view
 *   The view.
 * @param object $display
 *   The display belonging to the view.
 *
 * @return string
 *   An informative display string to used in the select box.
 */
function _views_merge_sort_get_display_name($view, $display) {
  $sort_names = array();
  $display_options = $display->display_options;
  foreach ($display_options['sorts'] as $sort_id => $sort_data) {
    $sort_name = NULL;
    if (isset($display_options['fields'][$sort_id])) {
      $sort_name = $display_options['fields'][$sort_id]['label'];
    }
    $sort_name = empty($sort_name) ? $sort_id : $sort_name;
    $sort_names[] = '"' . $sort_name . '"';
  }
  $display_name = $view->get_human_name() . ' (' . $display->display_title . ')';
  if (views_view_is_disabled($view)) {
    $display_name .= ' [' . t('disabled') . ']';
  }
  $display_name .= ', ' . t('sorted by') . ' ' . implode(' ' . t('then') . ' ', $sort_names);
  return $display_name;
}
