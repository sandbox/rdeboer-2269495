
VIEWS MERGE SORT
================
A Views plugin to replace on selected views the default "staggered" sort by
a "merged" sort.

Say you have a View of two content types.
Each has incarnations of a similar field, maybe a colour or a price.

For our purpose, let's say each has a distance field, like through the Geofield
module.

You want to sort all your locations (rows) by ascending distance to some
reference point. However, as each content type has its own distance field,
sorting will be on one distance field first, the other second:

            | Distance | Distance |
            | Field 1  | Field 2  |
----------------------------------|
location #1 |   0.3 km |    --    |
location #2 |   1.4 km |    --    |
location #3 |   2.3 km |    --    |
location #4 |     --   |  0.1 km  |
location #5 |     --   |  1.5 km  |

Not only is the order not what you want, if a limit is placed on the number of
items returned, say 3 in the example above, then the second content type may
not be represented in the output at all.

This module will put your sorting right. Flick it on for the View displays that
need it and your output will look like this:

            | Distance | Distance |
            | Field 1  | Field 2  |
----------------------------------|
location #4 |     --   |  0.1 km  |
location #1 |   0.3 km |    --    |
location #2 |   1.4 km |    --    |
location #5 |     --   |  1.5 km  |
location #3 |   2.3 km |    --    |

With this module enabled you can select the views that need the merge-sort
treatment here: admin/config/content/views_merge_sort
